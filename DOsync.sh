#!/bin/bash
SRC=$1
DST=$2
FOLDER=$3

echo $(date)

showhelp(){
    echo "\n\n############################################"
    echo "# DOsync.sh                                    #"
    echo "################################################"
    echo "\nThis script will sync files/folders to an S3 compatible store"
    echo "In order to work, this script needs the following three parameters in the listed order: "
    echo "\t- The full path for the folder or file you want to backup."
    echo "\t- The name of the Space where you want to store the backup at."
    echo "\t- The name of the folder in the Space where you want to store the backup at." 
    echo "Example: sh DOsync.sh ./testdir testSpace testFolder \n"
}

syncToSpace(){
    if s3cmd put --recursive -e $SRC s3://$DST/$FOLDER; then
        echo "\n##### Done syncing files from $SRC to s3://"$DST/$FOLDER" #####\n"
        return 0
    else
        echo "\n##### Failed to sync files to the Space #####\n"
        return 1
    fi
}

if [ ! -z "$SRC" ]; then
    syncToSpace
else
    showhelp
fi
