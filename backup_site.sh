#!/bin/sh
SITE=$1
DB=$2

echo $(date)

showhelp(){
    echo "\n\n############################################"
    echo "# backup_site.sh                            #"
    echo "############################################"
    echo "\nThis script will backup a site/database into a two compressed files and will store it in the site DO space."
    echo "In order to work, this script needs the following three parameters in the listed order: "
    echo "\t- The site you want to back up, i.e. deflumeri.com."
    echo "\t- The name for the database for the site you want to backup, i.e. deflumeri_com_prod\n"
    echo "Example: sh backup_ghost_site.sh deflumeri.com deflumeri_com_prod\n"
}

dbbackup(){
    if [ ! -z "$DB" ]; then
        mysqldump -u backup $DB > /tmp/$DB.backup.sql
        /home/deflume1/Scripts/DObackup.sh /tmp/$DB.backup.sql deflumeri $SITE-backups $DB.backup.sql
        rm /tmp/$DB.backup.sql
        return 0
    else
        return 1
    fi
}

sitebackup(){
    if [ ! -z "$SITE" ]; then
        /home/deflume1/Scripts/DObackup.sh /var/www/$SITE/ deflumeri $SITE-backups var.www.$SITE
        return 0
    else
        return 1
    fi
}

if [ ! -z "$SITE" ]; then
    if dbbackup; then
        sitebackup
    else    
        showhelp
    fi      
else    
    showhelp
fi
