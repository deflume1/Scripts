#!/bin/bash
SRC=$1
DST=$2
FOLDER=$3
GIVENNAME=$4
DOW=$(date +%u)

echo $(date)

showhelp(){
    echo "\n\n############################################"
    echo "# DObackup.sh                            #"
    echo "############################################"
    echo "\nThis script will backup files/folders into a single compressed file and will store it in the current folder."
    echo "In order to work, this script needs the following three parameters in the listed order: "
    echo "\t- The full path for the folder or file you want to backup."
    echo "\t- The name of the Space where you want to store the backup at."
    echo "\t- The name of the folder in the Space  where you want to store the backup at." 
    echo "\t- The name for the backup\n"
    echo "Example: sh DObackup.sh ./testdir testSpace testFolder backupdata\n"
}

tarandzip(){
    echo "\n##### Gathering files #####\n"
    if tar --ignore-failed-read --exclude='*node_modules*' --exclude='.git' -czvf /tmp/$GIVENNAME.$DOW.tar.gz $SRC; then
        echo "\n##### Done gathering files #####\n"
        return 0
    else
        echo "\n##### Failed to gather files #####\n"
        return 1
    fi
}

movetoSpace(){
    if s3cmd put -e /tmp/$GIVENNAME.$DOW.tar.gz s3://$DST/$FOLDER/; then
        echo "\n##### Done moving files to s3://"$DST/$FOLDER" #####\n"
        rm /tmp/$GIVENNAME.$DOW.tar.gz
        return 0
    else
        echo "\n##### Failed to move files to the Space #####\n"
        return 1
    fi
}

if [ ! -z "$GIVENNAME" ]; then
    if tarandzip; then
        movetoSpace
    else
        showhelp
    fi
else
    showhelp
fi
