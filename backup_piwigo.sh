#!/bin/sh
PIWIGO_HOME=$1 #/home/deflume1/piwigo
DB=$2 #piwigo

echo $(date)

showhelp(){
    echo "\n\n############################################"
    echo "# backup_piwigo.sh                            #"
    echo "############################################"
    echo "\nThis script will backup piwigo. It stores the database in a compressed file and will store it in the site DO space."
    echo "\nIt then syncs the gallery uploads to S3."
    echo "\nThe first parameter is the location of the piwigo installation data root."
    echo "\nThe second parameter is the name of your piwigo database in mysql."
    echo "Example: sh backup_piwigo.sh /home/deflume1/piwigo piwigo\n"
}

dbbackup(){
    if [ ! -z "$DB" ]; then
        mysqldump -u backup $DB > /tmp/$DB.backup.sql
        /home/deflume1/Scripts/DObackup.sh /tmp/$DB.backup.sql deflumeri $DB-backups $DB.backup.sql
        rm /tmp/$DB.backup.sql
        return 0
    else
        return 1
    fi
}

gallerysync(){
    if [ ! -z "$PIWIGO_HOME" ]; then
        /home/deflume1/Scripts/DObackup.sh $PIWIGO_HOME/data/gallery/upload deflumeri $DB-backups $DB-upload-backups
        return 0
    else
        echo "missing"
        return 1
    fi
}

if [ ! -z "$PIWIGO_HOME" ]; then
    if dbbackup; then
        gallerysync
    else    
        showhelp
    fi      
else    
    showhelp
fi
